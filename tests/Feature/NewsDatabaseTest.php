<?php

namespace Tests\Feature;

use App\Models\News;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class NewsDatabaseTest extends TestCase
{
    use DatabaseMigrations;


    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_the_application_returns_a_successful_response()
    {
        $news = News::factory()->create();
        $this->assertDatabaseHas('news', [
            'name' => $news->name,
            'description' => $news->description,
            'text' => $news->text
        ]);
    }
}
