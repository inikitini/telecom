<?php

namespace Tests\Feature;

use App\Models\News;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class NewsUpdateTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_the_application_returns_a_successful_response()
    {
        $news = News::factory()->create();

        Storage::fake('public');

        $file = UploadedFile::fake()->image('image.jpg');

        $response = $this->put('/api/news/'. $news->id, [
            'name' => $news->name,
            'description' => $news->description,
            'text' => $news->text,
            'publish_at' => $news->publish_at,
            'publishing' => $news->publishing,
            'file' => $file
        ]);

        Storage::disk('public')->assertExists($file->hashName());

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
            ]);
    }
}
