<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\News>
 */
class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id' => 1,
            'name' => $this->faker->title,
            'description' => $this->faker->text(255),
            'text' => $this->faker->text,
            'image' => $this->faker->image(storage_path('app/public'),500,500),
            'publish_at' => $this->faker->date,
            'publishing' => true
        ];
    }
}
