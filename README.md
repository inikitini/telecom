**Instructions to start using**

Clone repository

```
git clone https://gitlab.com/inikitini/telecom.git && cd telecom
```

Copy .env.example to .env

```
cp .env.example .env
```

install project without local packages

```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    composer install --ignore-platform-reqs
```

Running containers

```
./vendor/bin/sail up -d
```

if the build was run as root

```
docker exec -it telecom-laravel.test-1 /bin/bash
chown -R sail .
exit
```

Run migrations

```
./vendor/bin/sail artisan migrate
```

Create storage links

```
./vendor/bin/sail artisan storage:link
```

Run tests

```
./vendor/bin/sail artisan test
```

To stop containers

```
./vendor/bin/sail artisan stop
```
