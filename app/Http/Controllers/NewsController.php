<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json([
            'success' => true,
            'message' => 'News was successfully received',
            'data' => News::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreNewsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreNewsRequest $request)
    {
        if ($request->file('file')) {
            $filename = $request->file('file')->hashName();
            Storage::disk('public')->put($filename, file_get_contents($request->file('file')));
        }
        $publish_at = ($request->publishing) ? date('Y-m-d H:i:s') : null;
        $news = News::create($request->all() + ['image' => $filename, 'publish_at' => $publish_at]);

        return response()->json([
            'success' => true,
            'message' => 'News was saved successfully',
            'data' => $news,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\News $news
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(News $news)
    {
        return response()->json([
            'success' => true,
            'data' => $news,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateNewsRequest $request
     * @param \App\Models\News $news
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateNewsRequest $request, News $news)
    {
        if ($request->file('file')) {
            $filename = $request->file('file')->hashName();
            Storage::disk('public')->put($filename, file_get_contents($request->file('file')));
            File::delete(public_path('/storage/'.$news->image));
        }
        $publish_at = ($request->publishing) ? date('Y-m-d H:i:s') : null;
        $news->update($request->all() + ['image' => $filename, 'publish_at' => $publish_at]);

        return response()->json([
            'success' => true,
            'message' => 'News was update successfully',
            'data' => $news,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\News $news
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(News $news)
    {
        $news->delete();

        return response()->json([
            'success' => true,
            'message' => 'News was deleted successfully',
        ]);
    }
}
